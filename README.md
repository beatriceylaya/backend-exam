# Mini Blog Project
A mini blog project with authentication.

## Prerequisites
- Git
- Composer

## Environment Setup
Local Environment using Homestead
1. Download and install VirtualBox 6.x
2. Download and install Vagrant.
3. Clone the Laravel Homestead repository to your desired path.
```
git clone https://github.com/laravel/homestead.git ~/Homestead
```
4. Change directory to selected path and checkout release branch.
```
git checkout release
```
5. Run the following commands to create Homestead.yaml config file.
Mac
```
bash init.sh
```
Windows
```
init.bat
```
6. Open the initiated file from the Homestead directory via Notepad++ or whichever editor you prefer.
7. Configure your shared folder by editing the folders property of the Homestead.yaml. Files will be synced between your local machine and Homestead environment once done. You may also configure as many shared folders.
```
folders:
    - map: ~/var/www/folder1
      to: /path/to/your/host/machine1

    - map: ~/var/www/folder2
       to: /path/to/your/host/machine2
```
8. Configure your sites in `Homestead.yaml` 
```
sites:
    - map: homestead.test
      to: /home/vagrant/project1/public

    - map: mysite.dev
      to: /home/vagrant/project2/public
```
9. Add your sites to your hosts located at `C:\Windows\System32\drivers\etc`
```
192.168.10.10  homestead.test
```
10. You may now run the command `vagrant up` to start your project.
11. `vagrant ssh` to connect your homestead via SSH.
### Getting Started
1. Clone the project to your local machine.
2. Go to your project directory, open a terminal and run the following commands:
```
composer install
npm install
```
3. Create an `.env` file and configure your database and other environment configs. You may simply copy `env.example`.
```
cp .env.example .env
```
4. Generate application key.
```
php artisan key:generate
```
5. Run initial database migration.
```
php artisan migrate
```
6. Create OAuth2 private and public key by running
```
php artisan passport:install
```

For the frontend to start you may simply run `npm start`.

