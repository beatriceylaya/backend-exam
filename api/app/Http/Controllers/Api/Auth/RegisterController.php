<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Create a new user instance.
     *
     * @param Illuminate\Http\Request $request
     * @return \App\User
     */
    public function register(UserRequest $request)
    {
        $validatedData = $request->validated();
 
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password'])
        ]);

        $accessToken = $user->createToken('authToken')->accessToken;

        return $user;
    }
}
