<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'commentable_type',
        'commentable_id',
        'creator_id',
    ];

    /**
     * Set commentable type attribute to App\\Post.
     * 
     * @return voice
     */
    public function setCommentableTypeAttribute()
    {
        $this->attributes['commentable_type'] = "App\\Post";
    }

    /**
     * Retrieves user who commented.
     * 
     * @return App\Models\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieve parent commentable.
     * 
     * @return App\Models\Post
     */
    public function commentable()
    {
        return $this->morphTo();
    }   
}
