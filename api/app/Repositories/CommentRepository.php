<?php

namespace App\Repositories;

use Auth;
use App\Models\Comment;
use App\Http\Resources\CommentResource;

class CommentRepository
{
    /** 
     * @var $comment
     */
    protected $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Create comment to post.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        $comment = $this->comment->create([
            'body' => $data['body'],
            'creator_id' => Auth::user()->id,
            'commentable_type' => "App\\Post",
            'commentable_id' => $data['post']['id']
        ]);

        return $comment;
    }

    /**
     * Update comment of comment.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function update($data, $comment)
    {
        $comment = $comment->update($data);

        return $comment;
    }

    /**
     * Delete comment from comment.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function delete($data)
    {
        $comment = $this->comment->find($data->id);
        $comment->delete();

        return $comment;
    }

    /**
     * Retrieve all comments from comment.
     * 
     * @return \Illuminate\Http\Response
     */
    public function findAll()
    {
        $comments = $this->comment->paginate();

        return new CommentResource($comments);
    }
}
