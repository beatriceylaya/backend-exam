<?php

namespace App\Repositories;

use Auth;
use App\Models\Post;
use InvaliInvalidArgumentException;
use App\Http\Resources\PostResource;

class PostRepository
{
    /** 
     * @var $post
     */
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Create post.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        $post = $this->post->create([
            'image' => $data['image'],
            'title' => $data['title'],
            'content' => $data['content'],
            'user_id' => Auth::user()->id
        ]);

        return $post;
    }

    /**
     * Update post.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function update($data, $post)
    {
        $post = $this->post->find($post->id);

        $post->title = $data['title'];
        $post->slug = str_slug($data['title']);
        $post->update();

        return $post;
    }

    /**
     * Delete post.
     * 
     * @param App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function delete($post)
    {
        $post = $this->post->find($post->id);
        $post->delete();

        return $post;
    }

    /**
     * Retrieve all post.
     * 
     * @return \Illuminate\Http\Response
     */
    public function findAll()
    {
        $posts = $this->post->paginate();

        return new PostResource($posts);
    }

    /**
     * Retrieve post by id.
     * 
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function findById($id)
    {
        $post = $this->post->find($id);

        return $post;
    }
}
