<?php

namespace App\Services;

use DB;
use \Exception;
use App\Models\Comment;
use InvalidArgumentException;
use App\Repositories\CommentRepository;

class CommentService {

    /**
     * @var $commentRepository;
     */
    protected $commentRepository;

    /**
     * Initialize class.
     */
    public function __construct()
    {
        $this->commentRepository = new CommentRepository(new Comment());
    }

    /**
     * Create comment.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        DB::beginTransaction();
        
        try {
            $comment = $this->commentRepository->create($data);
        } catch (Exception $e) {
            DB::rollBack();
            
            throw new Exception($e->getMessage());
        }

        DB::commit();

        return $comment;
    }

    /**
     * Update post.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function update($data)
    {
        DB::beginTransaction();

        try {
            $post = $this->commentRepository->update($data);
        } catch (Exception $e) {
            DB::rollBack();

            throw new InvalidArgumentException('Unable to delete comment.');
        }

        DB::commit();

        return response()->json(['data' => $post]);
    }

    /**
     * Delete post.
     * 
     * @param array $data
     */
    public function delete($data)
    {
        DB::beginTransaction();

        try {
            $comment = $this->commentRepository->delete($data);
        } catch (Exception $e) {
            DB::rollBack();
        }

        DB::commit();

        return $comment;
    }


    /**
     * Retrieve comments from post.
     * 
     * @return \Illuminate\Http\Response
     */
    public function findAll() 
    {
        $comments = $this->commentRepository->findAll();

        return $comments;
    }
}
