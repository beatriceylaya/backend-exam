<?php

namespace App\Services;

use DB;
use \Exception;
use App\Models\Post;
use InvalidArgumentException;
use App\Repositories\PostRepository;
use Illuminate\Support\Facades\Validator;

class PostService {

    /**
     * @var $postRepository;
     */
    protected $postRepository;

    /**
     * Initialize class.
     */
    public function __construct()
    {
        $this->postRepository = new PostRepository(new Post());
    }

    /**
     * Create post.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->postRepository->create($data);
        } catch (Exception $e) {
            DB::rollBack();
            
            throw new Exception($e->getMessage());
        }

        DB::commit();

        return $post;
    }

    public function show(Post $post)
    {
        $post = $this->postRepository->findById($post->id);

        return $post;
    }

    /**
     * Update post.
     * 
     * @param array $data
     * @return \Illuminate\Http\Response
     */
    public function update($data, $post)
    {
        DB::beginTransaction();

        try {
            $post = $this->postRepository->update($data, $post);
        } catch (Exception $e) {
            DB::rollBack();

            throw new InvalidArgumentException('Unable to update post.');
        }

        DB::commit();

        return response()->json(['data' => $post]);
    }

    /**
     * Delete post.
     * 
     * @param App\Models\Post $post
     * @return bool
     */
    public function delete($post)
    {
        DB::beginTransaction();

        try {
            $post = $this->postRepository->delete($data);
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception('Unable to delete post.');
        }

        DB::commit();

        return $post;
    }

    /**
     * Retrieve all posts.
     * 
     * @return 
     */
    public function findAll()
    {
        $posts = $this->postRepository->findAll();

        return $posts;
    }

    /**
     * Retrieve post.
     * 
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function findById($id) 
    {
        
    }
}
