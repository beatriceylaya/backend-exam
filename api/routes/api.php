<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// authentication routes
Route::post('/register', 'Api\Auth\RegisterController@register')->name('register');
Route::post('/login', 'Api\Auth\LoginController@login')->name('login');

// guest routes
Route::prefix('posts')->group(function() {
    Route::get('/', 'Api\PostController@findAll');
    Route::get('/{post}', 'Api\PostController@show');
    Route::get('/{post}/comments', 'Api\CommentController@findAll');
});

Route::middleware('auth:api')->group(function() {
    Route::post('logout', 'Api\Auth\LoginController@logout')->name('logout');
    
    Route::prefix('posts')->group(function() {
        // post routes
        Route::post('/', 'Api\PostController@store');
        Route::patch('/{post}', 'Api\PostController@update');
        Route::delete('/{post}', 'Api\PostController@destroy');

        // comment routes
        Route::post('/{post}/comments', 'Api\CommentController@store');
        Route::patch('/{post}/comments/{id}', 'Api\CommentController@update');
        Route::delete('/{post}/comments/{id}', 'Api\CommentController@destroy');
    });

});
